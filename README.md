# Statistical Report for Superstore
A statistical report for a large retail store based on a sample dataset with 22 variables. This project has the report itself as well as the FinalData.sas file I used to generate all the graphs and queries for the report.

## Major Sections
The report contains 5 major sections. They will be outlined below. A link to a google doc of the report is here: https://docs.google.com/document/d/1bI2vX25AMgtOoapESNcT6MGN0YZybvS8adNe2lC0j50/edit?usp=sharing

### Introdcution
This section gives an overview of the findings of the report and motivates the findings which are derrived later on in the paper.

### Dataset Description
This section describes the dataset that was analyzed. It also highlights the variables that were most looked at and describes the methods used to clean up the dataset that was given.

### Visualization and Explanation of Findings
This section is the largest and it includes the explanation for my conclusions using text and graphs. The graphs were generated using SAS and the code for each graph is included later on in the appendix.

### Regression and Analysis
This section includes the regression I attempted in order to explain the profits the company experiences based on 3 different numerical variables.

### Conclusions and Reflection
This section includes my suggestions to the Superstore company based on my findings. It also includes a reflection on the limitations of the investigation and things that can be improved on in the future.

### Appendix
This section is the appendix for all of the code I used in the report. It seperates each code segment into categories which highlight the purpose of each code segment. 

