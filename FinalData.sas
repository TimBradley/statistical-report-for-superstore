*imported from the superstore csv file;
data RawData;
set TIMBLIB.store;
run; 

*map used for it's state location data;
data Map;
set maps.us;
rename state = statekey;
run;

*map used for it's state key value;
data Map2;
set maps.us2;
run;

proc sql;
create table catsummary as
select category, sum(profit) as totprof
from rawdata
group by Category
;
quit;

proc print data=catsummary;
print;

*generates data used for everything except the gmap function;
proc sql;
create table discounted as
select category, subcategory, sales, profit, mean(profit) as avgprofit, discount, quantity, sum(profit) as TotalProfit,
round(discount, 0.2) as discountclass, segment 
from RawData
group by subcategory
order by Category, sales, avgprofit
;
quit;

*creates a table from our sample data including the total profit for each state;
proc sql;
create table mapdata as
select sales, profit, state, sum(profit) as stateprofit
from RawData
group by state
order by state, profit
;
quit;

*adds the statekey variable to the table;
proc sql;
alter table mapdata 
	add statekey NUM(2);
quit;

*gives the statekey a value corresponding to the map's state value, this process converts state from a
string to a value we can use in the mapping process;
proc sql;
update mapdata as u
set statekey = (select state from work.Map2 as s where mapdata.state = s.statename)
where state in (select statename from work.Map2);
quit;	

*used to format the map data;
proc format;
value profitformat low- -10000="min to -10000"
				   -9999-0="-10000 to 0"
				   1-5000="0 to 5000"
				   5001-10000="5000 to 10000"
				   10001-20000="10000 to 20000"
				   20001-high="20000 to max";
run;


*gmap function which shows the total profit across us states;
proc gmap map = map data= mapdata;
title "Map of Total Profits for US States";
format stateprofit profitformat.;
id statekey;
choro stateprofit / levels=all;
run;

*boxplot graph of profit vs category;
proc sgplot data=discounted;
title "Boxplot of Profits by Category";
vbox profit / category=category nooutliers;
yaxis values=(-150 to 200 by 25);
refline 0;
run;

*bar graph of category vs total profit grouped by subcategory;
proc sgplot data=discounted;
title "Distribution of Profits for Subcategories";
vbar category / response=TotalProfit group=subcategory groupdisplay=cluster;
yaxis label="Total Profit";
run; 

*plot of average profit by customer type;
proc sgplot data=discounted;
title "Average Profit  by Customer Type";
vbar segment / response=profit stat=mean;
run; 

*regression step with model (profit ~ sales, discount, quantity);
proc reg data=discounted plots(MAXPOINTS=10000);
title "Regression Report of Profit vs Sales & Discount";
model profit = sales discount quantity;
run;
quit;

*Macro to cycle through and create a panel plot for each category at each subcategory for different discount levels;
%Macro catdata(category);
proc sgpanel data=discounted;
Title "Total Profits for &category at Various Discount Levels by Subcategory";
where Category = "&category";
panelby subcategory / onepanel novarname;
vbar discountclass / response=profit stat=mean fill;
run;
%MEND catdata;

%catdata(Furniture)
%catdata(Office Supplies)
%catdata(Technology)